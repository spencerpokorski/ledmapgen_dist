#include <iostream>
#include <Magick++.h>
#include <vector>
#include <sstream>
using namespace std;
using namespace Magick;

class raster {
    public:
        int width; // width of the raster in px
        int height; // height of the raster in px
        string rastersize; //size of the raster in pixels
        string rastercolor; //color of the raster background, or "canvas"
        string rasterName; //filename of the returned image TODO: implement
        int quantity; //number of screens in one raster
};

class panel {
    public:
        int width; //width of the LED product in px
        int height; //height of the LED product in px
        string panelsize; //size of the raster in pixels
};

class screen {
    public:
        int width; //width of the LED screen in tile count
        int height; //height of the LED screen in tile count
        int origin_x; //x origin point
        int origin_y; //y origin point
        int pxwide; //width of the LED screen in pixels
        int pxhigh; //height of the LED screen in pixels
        
        //TODO implement following??
        //Coordinate origin; //Magick++ Cooardinate class is just int x, int y
};

int main() {
    InitializeMagick(NULL);

    raster rasta;

    cout << "Raster width:" << endl;
    cin >> rasta.width;
    cout << endl << "Raster height:" << endl;
    cin >> rasta.height;
    cout << endl << "Number of screens on this raster:" <<endl;
    cin >> rasta.quantity;
    
    
    // Vectors so we can have dynamic number of screens
    vector<screen> screens;
    vector<panel> panels;
    for (int i = 0; i < rasta.quantity; i++){
        screens.push_back(screen());
        panels.push_back(panel());
    };

    Geometry rg(rasta.width,rasta.height);
    ColorRGB rbg(0,0,0);
    ColorRGB abg(85/255,30/255,90/255);
    ColorRGB bbg(114/255, 36/255, 108/255);

    Image rout(rg, rbg);

    for (int i = 0; i < screens.size(); i++){

        cout << endl << "-- Settings for screen " << i + 1 << " --" << endl;
        cout << endl << "Width of LED product (in pixels):" << endl;
        cin >> panels[i].width;
        cout << endl << "Height of LED product (in pixels):" << endl;
        cin >> panels[i].height;
        cout << endl << "How many panels wide is the screen?" << endl;
        cin >> screens[i].width;
        cout << endl << "How many panels high is the screen?" << endl;
        cin >> screens[i].height;
        cout << endl << "Screen x origin:" << endl;
        cin >> screens[i].origin_x;
        cout << endl << "Screen y origin:" << endl;
        cin >> screens[i].origin_y;

        int sw = screens[i].width;
        int pw = panels[i].width;
        int sh = screens[i].height;
        int ph = panels[i].height;
        int x = screens[i].origin_x;
        int y = screens[i].origin_y;
        int panelcount = sw*sh;

        screens[i].pxwide = sw * pw;
        screens[i].pxhigh = sh * ph;

        // row loop
        for (int h=0; h < sh; h++){
            
            // column loop
            for (int f=0; f < sw; f++){
                double tlx = x+(pw*f);
                double tly = y+(ph*h);
                double brx = tlx+pw-1;
                double bry = tly+ph-1;
                // this is where the checkerboard magic happens
                // say h is 0 and f is 0 -> c is 0. 0 % 2 = 0, go red
                // say h is 0 and f is 1 -> c is 1. 1 % 2 != 0, go blue
                // say h is 2 and f is 2 -> c is 4, 4 % 2 = 0, go red
                // say h is 2 and f is 3 -> c is 5, 5 % 2 != 0, go blue
                int c = f+h;
                if ( c % 2 == 0 ){
                    rout.fillColor("teal");
                    rout.strokeColor("orange");
                }
                else {
                    //reference GraphicsMagick API for predefined colors
                    rout.fillColor("navy");
                    rout.strokeColor("mediumspringgreen");
                };
                rout.draw(DrawableRectangle(tlx,tly,brx,bry));
                
                //there is some spooky magic here!
                //these calls should only produce 3 lines, not 4 (like they do).
                rout.draw(DrawableLine(tlx,tly,tlx+pw-1,tly));
                rout.draw(DrawableLine(tlx+pw-1,tly,tlx+pw-1,tly+ph-1));
                rout.draw(DrawableLine(tlx+pw-1,tly+ph-1,tlx,tly+ph-1));

                //text stuff - TODO: find more dynamic solution

                ostringstream titles;
                //worked on hi-res tiles 
                //titles << "Col: " << f+1 << endl << "Row: " << h+1;
                titles << f+1 << "-" << h+1 << endl
                    //<< "(" << tlx << "," << tly << ")";
                    << "x: " << tlx << endl
                    << "y: " << tly;
                string tit = titles.str();

                rout.fillColor("white");
                rout.font("helvetica");
                rout.strokeWidth(0);
                rout.strokeColor(Color(0,0,0,MaxRGB));
                rout.fontPointsize(14);
                rout.draw(DrawableText((tlx+(pw/4)),(tly+(ph/4)), tit));
            };
        };

    };
                
    // white outer border
    for (int i = 0; i < screens.size(); i++){

        //HACK HACK HACK YOU ARE A HACK
        int sw = screens[i].width;
        int pw = panels[i].width;
        int sh = screens[i].height;
        int ph = panels[i].height;
        int x = screens[i].origin_x;
        int y = screens[i].origin_y;
        int panelcount = sw*sh;

        rout.strokeColor("white");
        rout.fillColor(Color(0,0,0,MaxRGB));
        rout.draw(DrawableRectangle(x,y,(x+screens[i].pxwide)-1,(y+screens[i].pxhigh)-1));
    };


    rout.write("output.png");

}
